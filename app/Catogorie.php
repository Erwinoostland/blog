<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catogorie extends Model
{
    public function posts()
    {
        return $this->hasMany('App\Post', 'catogorie_id', 'id');
    }
}
