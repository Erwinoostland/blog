<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function catogorie()
    {
        return $this->belongsTo('App\Catogorie', 'catogorie_id', 'id');
    }
}
