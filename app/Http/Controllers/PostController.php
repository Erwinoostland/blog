<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function add(Request $request)
    {
        $newpost = new Post;
        $newpost->name = $request->input('name');
        $newpost->catogorie_id = $request->input('catogorie');
        $newpost->post = $request->input('post');

        if ($newpost->save()) {
            \Session::flash('Success', 'Post successfull');
        } else {
            \Session::flash('Error', 'Post unsuccessfull');
        }
        return redirect('/');

    }

    public function delete($id)
    {
        $deletepost = Post::find($id);

        if ($deletepost->delete()) {
            \Session::flash('Success', 'Delete successfull');
        } else {
            \Session::flash('Error', 'Delete unsuccessfull');
        }
        return redirect('/');
    }

    public function edit(Request $request, $id)
    {
        $editpost = Post::find($id);

        $editpost->name = $request->input('name');
        $editpost->catogorie_id = $request->input('catogorie');
        $editpost->post = $request->input('post');

        if ($editpost->update()) {
            \Session::flash('Success', 'Edit successfull');
        } else {
            \Session::flash('Error', 'Edit unsuccessfull');
        }
        return redirect('/');


    }
}
