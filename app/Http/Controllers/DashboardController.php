<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catogorie;

class DashboardController extends Controller
{
    public function index(){

        $catogories = Catogorie::all();
        return view('dashboard/dashboard', compact('catogories'));
    }
}
