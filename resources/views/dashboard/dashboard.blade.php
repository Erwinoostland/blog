@extends('layouts.layouts')

@section('content')
    <section class="row">
        <section class="col-md-12">
            @if (\Session::has('Success'))
                <section class="alert alert-success" role="alert">
                    {{\Session::get('Success')}}
                </section>
            @elseif(\Session::has('Error'))
                <section class="alert alert-danger" role="alert">
                    {{\Session::get('Error')}}
                </section>
            @endif
            <section class="card">
                <section class="card-header">
                    <h4 class="piethaan">Blog</h4>
                    <a class="btn btn-dark standardbutton" data-toggle="modal" data-target="#addPost"
                    >Add post</a>
                </section>
                <section class="card-body">
                    <section class="row">
                        @foreach($catogories as $catogorie)

                            <section class="col-md-6">
                                <section class="card mt-2">
                                    <section class="card-header">
                                        <h4>{{$catogorie->name}}</h4>
                                    </section>
                                    <section class="card-body">
                                        @foreach($catogorie->posts as $post)
                                            <section class="card">
                                                <section class="card-body">
                                                    <p>{{$post->post}}</p>
                                                    <a href={{url("/delete/".$post->id)}}>
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                    <a href="" data-target="#editPost{{$post->id}}" data-toggle="modal">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <i style="float: right;">{{$post->name}} {{date('d-m-Y H:i', strtotime($post->created_at))}}</i>
                                                </section>
                                            </section>

                                            {{--Edit modal--}}
                                            <section class="modal fade" id="editPost{{$post->id}}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="editPostLabel"
                                                     aria-hidden="true">
                                                <section class="modal-dialog" role="document">
                                                    <section class="modal-content">
                                                        <section class="modal-header">
                                                            <h5 class="modal-title" id="editPostLabel">Edit</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </section>
                                                        <form method="post" action="{{url("/edit/".$post->id)}}">
                                                            {{csrf_field()}}
                                                            <section class="modal-body">
                                                                <section class="form-group">
                                                                    <input class="form-control" type="text" name="name"
                                                                           value="{{$post->name}}">
                                                                </section>
                                                                <section class="form-group">
                                                                    <select class="form-control" name="catogorie">
                                                                        <option value="{{$post->catogorie_id}}" selected>{{$post->catogorie->name}}
                                                                        </option>
                                                                        @foreach($catogories->except($post->catogorie_id) as $catogorie)
                                                                            <option value="{{$catogorie->id}}">{{$catogorie->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </section>
                                                                <section class="form-group">
                            <textarea class="form-control textarea" name="post"
                            >{{$post->post}}</textarea>
                                                                </section>
                                                            </section>
                                                            <section class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">Edit
                                                                </button>
                                                            </section>
                                                        </form>
                                                    </section>
                                                </section>
                                            </section>

                                        @endforeach
                                    </section>
                                </section>
                            </section>

                        @endforeach
                    </section>
                </section>

            </section>
        </section>
    </section>

    {{--Add modal--}}
    <section class="modal fade" id="addPost" tabindex="-1" role="dialog" aria-labelledby="addPostLabel"
             aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="addPostLabel">Post!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>
                <form method="post" action="{{url("/addPost")}}">
                    {{csrf_field()}}
                    <section class="modal-body">
                        <section class="form-group">
                            <input class="form-control" type="text" name="name" placeholder="Name">
                        </section>
                        <section class="form-group">
                            <select class="form-control" name="catogorie">
                                <option value="" selected disabled>Choose one</option>
                                @foreach($catogories as $catogorie)
                                    <option value="{{$catogorie->id}}">{{$catogorie->name}}</option>
                                @endforeach
                            </select>
                        </section>
                        <section class="form-group">
                            <textarea class="form-control textarea" placeholder="Type something..." name="post"
                                      ></textarea>
                        </section>
                    </section>
                    <section class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </section>
                </form>
            </section>
        </section>
    </section>


@endsection